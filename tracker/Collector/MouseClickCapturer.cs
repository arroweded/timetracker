﻿using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using tracker.Collectors;

namespace tracker.Collector
{
    class MouseClickCapturer : AbstractCollector
    {
        private MouseHookListener mouseListener;
        private long clicks = 0;

        public override byte[] FireEvent()
        {
            long myKeys = 0;

            if (mouseListener != null)
            {
                lock (mouseListener)
                {
                    myKeys = clicks;
                    clicks = 0;
                }
            }

            return new UTF8Encoding(true).GetBytes(String.Format("{0}", myKeys));
        }

        public override string GetKey()
        {
            return "MouseClick";
        }

        public override void Setup()
        {
            mouseListener = new MouseHookListener(new GlobalHooker());

            // The listener is not enabled by default
            mouseListener.Enabled = true;

            // Set the event handler
            // recommended to use the Extended handlers, which allow input suppression among other additional information
            mouseListener.MouseClick += MouseListener_MouseClick;
        }

        private void MouseListener_MouseClick(object sender, MouseEventArgs k)
        {
            lock (mouseListener)
            {
                clicks++;
            }
        }
    }
}
