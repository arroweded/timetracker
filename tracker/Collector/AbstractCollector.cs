﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tracker.Collectors
{
    public abstract class AbstractCollector
    {
        private string key;

        /// <summary>
        /// Called to register an event
        /// </summary>
        public abstract byte[] FireEvent();

        /// <summary>
        /// Called to initiate
        /// </summary>
        public abstract void Setup();

        /// <summary>
        /// Return the key from the generator
        /// This will be used later on when trying to aggregate information
        /// </summary>
        public abstract string GetKey();

        /// <summary>
        /// Set the encryption key
        /// </summary>
        public string EncryptionPassword {
            get { return key; }
            set { key = value;} }
    }
}
