﻿using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using tracker.Collectors;

namespace tracker.Collector
{
    class ContextSwitchListener : AbstractCollector
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        private KeyboardHookListener keyboardListener;
        private MouseHookListener mouseListener;
        private IntPtr lastActiveWindowhWnd;
        private int contextSwitches = 0;
        private int lastKey = 0;

        public override byte[] FireEvent()
        {
            long myKeys = 0;

            if (keyboardListener != null)
            {
                lock (keyboardListener)
                {
                    myKeys = contextSwitches;
                    contextSwitches = 0;
                }
            }

            return new UTF8Encoding(true).GetBytes(String.Format("{0}", myKeys));
        }

        public override string GetKey()
        {
            return "ContextSwitches";
        }

        public override void Setup()
        {
            keyboardListener = new KeyboardHookListener(new GlobalHooker());
            mouseListener = new MouseHookListener(new GlobalHooker());

            // The listener is not enabled by default
            keyboardListener.Enabled = true;
            mouseListener.Enabled = true;

            // Set the event handler
            // recommended to use the Extended handlers, which allow input suppression among other additional information
            keyboardListener.KeyUp += KeyListener_KeyUp;
            mouseListener.MouseClick += MouseListener_MouseClick;
        }

        private void KeyListener_KeyUp(object sender, KeyEventArgs k)
        {
            lock(keyboardListener)
            {
                KeyEventArgsExt ext = k as KeyEventArgsExt;

                // if pressing alt tab, treat as a switch
                // even if the user returns to the same widow the user has at 
                // least lost focus

                //tab will be lifted first, and then alt after that
                if (ext.KeyValue == 164 && lastKey.Equals('\t'))
                    contextSwitches++;

                lastKey = k.KeyValue;
            }
        }

    
        private void MouseListener_MouseClick(object sender, MouseEventArgs k)
        {
            lock (mouseListener)
            {
                IntPtr foreground = GetForegroundWindow();
                if (! foreground.Equals(lastActiveWindowhWnd))
                {
                    contextSwitches++;
                    lastActiveWindowhWnd = foreground;
                }
            }
        }
    }
}
