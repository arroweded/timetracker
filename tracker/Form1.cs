﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Security.Cryptography;

namespace tracker
{
    public partial class Form1 : Form
    {
        private Thread capturer;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Icon = this.Icon;
                notifyIcon1.Visible = true;

                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
                notifyIcon1.Icon = null;
            }
        }

        private void eventUpdate(object sender, tracker.Ticker.EventCollectedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                textBox2.Text = e.GetInfo();
                notifyIcon1.Text = e.GetInfo().Substring(0, 63);
            });
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //restore the form
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Ticker ticker = new Ticker();
            ticker.eventCollected += eventUpdate;

            capturer = new Thread(new ThreadStart(ticker.loop));
            capturer.IsBackground = true;

            // calculate an encryption key
            ticker.EncryptionKey = txtPassword.Text;

            capturer.Start();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var pattern = Properties.Settings.Default.DesktopCapturePath;
            var filename = String.Format(pattern, DateTime.Now.ToString("yyyyMMdd"));

            // setup file output
            FileStream fs = new FileStream(filename, FileMode.Open);

            try
            {
                // setup encryption
                RijndaelManaged sym = new RijndaelManaged();
                sym.Mode = CipherMode.CBC;
                byte[] Salt = new byte[16];
                System.Random rnd = new System.Random();

                Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(txtPassword.Text, Salt);
                int KeyStrengthInBytes = sym.KeySize / 8;
                sym.Key = rfc2898.GetBytes(KeyStrengthInBytes);
                CryptoStream CryptStream = new CryptoStream(fs, sym.CreateDecryptor(), CryptoStreamMode.Read);

                byte[] b = new byte[2];

                //if we have part of the signature
                byte[] sig = new byte[]{255, 217}; //C0
                bool partial = false;
                long bread = 0;
                
                while (CryptStream.Read(b, 0, 2) == 2)
                {
                    bread += 2;

                    if (b == sig)
                    {
                        //match
                        partial = false;
                    }
                    else if (partial && b[0] == sig[1])
                    {
                        //match
                        partial = false;
                    }
                    else if (b[1] == sig[0])
                    {
                        partial = true;
                    }
                    else
                    {
                        partial = false;
                    }
                }

            }
            finally
            {
                fs.Close();
            }
        }
    }
}
