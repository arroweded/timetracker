﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using tracker.Collectors;
using NYurik.TimeSeriesDb;
using NYurik.TimeSeriesDb.Serializers;
using System.IO;
using tracker.Collector;

namespace tracker
{
    class Ticker 
    {
        HashSet<AbstractCollector> collectors = new HashSet<AbstractCollector>();
        
        public Ticker()
        {
            collectors.Add(new ScreenCapturer());
            collectors.Add(new WindowTitleCapturer());
            collectors.Add(new KeyboardListener());
            collectors.Add(new MouseClickCapturer());
            collectors.Add(new AudioTrackCapturer());
            collectors.Add(new ContextSwitchListener());

            //initial setup
            foreach (var g in collectors)
            {
                g.Setup();
            }
        }

        public string EncryptionKey
        {
            set
            {
                foreach (var g in collectors)
                {
                    g.EncryptionPassword = value;
                }
            }
        }
    
        public void loop()
        {
            while(true)
            {
                String lastText = "";

                string path = String.Format(Properties.Settings.Default.DataCapturePath, DateTime.Now.ToString("yyyyMMdd"));
                
                using (FileStream fs = File.Open(path, FileMode.Append, FileAccess.Write, FileShare.None))
                {
                    Byte[] header = new UTF8Encoding(true).GetBytes(String.Format("[{0}]\n", DateTime.Now.ToString("yyyyMMddhhmmss")));
                    fs.Write(header, 0, header.Length);

                    foreach (var g in collectors)
                    {
                        byte[] data = g.FireEvent();
                        if (data != null && data.Length > 0)
                        {
                            byte[] key = new UTF8Encoding(true).GetBytes(String.Format("{0} = ", g.GetKey()));
                            fs.Write(key, 0, key.Length);
                            fs.Write(data, 0, data.Length);
                            byte[] b = new byte[] {(byte)'\n'};
                            fs.Write(b, 0, 1);
                            
                            lastText += string.Format("{0} {1}\r\n", new UTF8Encoding(true).GetString(key), new UTF8Encoding(true).GetString(data));
                        }
                    }
                }

                eventCollected(this, new EventCollectedEventArgs(lastText));
                
                Thread.Sleep(Properties.Settings.Default.Interval);
            }
        }

        public event EventCollected eventCollected;
        public delegate void EventCollected(object source, EventCollectedEventArgs e);
        public class EventCollectedEventArgs : EventArgs
        {
            private string EventInfo;
            public EventCollectedEventArgs(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }

    }
}
