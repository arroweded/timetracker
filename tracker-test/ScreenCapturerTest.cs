﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using tracker.Collectors;
using System.IO;
using System.Security.Cryptography;

namespace tracker_test
{
    [TestClass]
    public class ScreenCapturerTest
    {
        private static MemoryStream testStream = new MemoryStream();
        
        [TestMethod]
        public void TestDecryption()
        {
            ScreenCapturer s = new ScreenCapturer();
            s.Setup();
            s.EncryptionPassword = @"ASD";
            s.stream = MemoryOutputStream;

            s.FireEvent();

            byte[] eof = new byte[] {0xFF, 0xD9};
            int match = 0;
            int found = 0;

            RijndaelManaged sym = new RijndaelManaged();
            sym.Mode = CipherMode.CBC;
            byte[] Salt = new byte[16] { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
            byte[] Key = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
            byte[] IV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

            System.Random rnd = new System.Random(); 

            Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(s.EncryptionPassword, Salt);
            int KeyStrengthInBytes = sym.KeySize / 8;
            sym.Key = rfc2898.GetBytes(KeyStrengthInBytes);
            CryptoStream CryptStream = new CryptoStream(new MemoryStream(testStream.ToArray()), sym.CreateDecryptor(Key, IV), CryptoStreamMode.Read);
            
            FileStream fos = new FileStream("out.jpg", FileMode.Create);
            
            while (true)
            {
                byte[] buf = new byte[1];
                int readByte = CryptStream.Read(buf, 0, buf.Length);
                if (readByte == 0)
                    break;

                if (buf[0] == eof[match])
                {
                    if (match == eof.Length-1)
                    {
                        found++;
                        match = 0; //reset matching
                    }
                    else
                    {
                        match++;
                    }
                }
                else
                {
                    match = 0;
                }

                fos.Write(buf, 0, buf.Length);
                fos.Flush();
            }

            fos.Close();
            Assert.AreEqual(found, 1, "Expected 1 image");
        }

        public static Stream MemoryOutputStream()
        {
            // setup file output
            return testStream;
        }
    }
}
