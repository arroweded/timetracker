﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace tracker.Collectors
{
    class AudioTrackCapturer : AbstractCollector
    {
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(String className, String windowName);

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        [DllImport("user32.dll")]
        static extern int GetWindowTextLength(IntPtr hWnd);

        public override byte[] FireEvent()
        {
            IntPtr foreground = FindWindow(Properties.Settings.Default.AudioProgramWindow, null);

            Regex pattern = new Regex(Properties.Settings.Default.AudioProgramTitleRegexp,
                        RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(150));

            if (foreground!=null && foreground.ToInt64() > 0)
            {
                int t = GetWindowTextLength(foreground);

                StringBuilder windowText = new StringBuilder(t+2);

                int ret = GetWindowText(foreground, windowText, t+2);

                if (ret > 0)
                {
                    Match m = pattern.Match(windowText.ToString());
                    if (m.Success)
                        return new UTF8Encoding(true).GetBytes(pattern.Match(windowText.ToString()).Result("${name}"));
                }
            }

            return null;
        }

        public override void Setup()
        {

        }

        public override string GetKey()
        {
            return "AudioTrack";
        }
    }
}
