﻿using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using tracker.Collectors;

namespace tracker.Collector
{
    class KeyboardListener : AbstractCollector
    {
        private KeyboardHookListener keyboardListener;
        private long keyPresses = 0;

        public override byte[] FireEvent()
        {
            long myKeys = 0;

            if (keyboardListener != null)
            {
                lock (keyboardListener)
                {
                    myKeys = keyPresses;
                    keyPresses = 0;
                }
            }

            return new UTF8Encoding(true).GetBytes(String.Format("{0}", myKeys));
        }

        public override string GetKey()
        {
            return "Keyboard";
        }

        public override void Setup()
        {
            keyboardListener = new KeyboardHookListener(new GlobalHooker());

            // The listener is not enabled by default
            keyboardListener.Enabled = true;

            // Set the event handler
            // recommended to use the Extended handlers, which allow input suppression among other additional information
            keyboardListener.KeyPress += KeyListener_KeyPress;
        }

        private void KeyListener_KeyPress(object sender, KeyPressEventArgs k)
        {
            lock(keyboardListener)
            {
                keyPresses++;
            }
        }
    }
}
