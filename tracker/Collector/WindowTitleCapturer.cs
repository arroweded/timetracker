﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;


namespace tracker.Collectors
{
    class WindowTitleCapturer : AbstractCollector
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        [DllImport("user32.dll")]
        static extern int GetWindowTextLength(IntPtr hWnd);

        public override byte[] FireEvent()
        {
            IntPtr foreground = GetForegroundWindow();
            
            int t = GetWindowTextLength(foreground);

            if (t > 0)
            {
                StringBuilder windowText = new StringBuilder(t+2);

                int ret = GetWindowText(foreground, windowText, t+2);

                if (ret > 0)
                    return new UTF8Encoding(true).GetBytes(windowText.ToString());
            }

            return null;
        }

        public override void Setup()
        {

        }

        public override string GetKey()
        {
            return "WindowTitle";
        }
    }
}
